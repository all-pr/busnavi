package com.busnavi.core.service;

import com.google.common.collect.Sets;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
@Slf4j
public class LinesEngine {

    private static Map<Integer, Map<Integer, Integer>> store;

    @PostConstruct
    public void setUp() {
       store = createStop2LineStruct("lines.txt");
    }


    private static Map<Integer, Map<Integer, Integer>> createStop2LineStruct(String fileName) {
        long startTime = System.currentTimeMillis();
        Map<Integer, Map<Integer, Integer>> res = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\\s+");

                int id = Integer.parseInt(parts[0]);
                for (int iStop = 1; iStop < parts.length; iStop++) {
                    int stop = Integer.parseInt(parts[iStop]);

                    Map<Integer, Integer> stop2Position = res.computeIfAbsent(stop, (s) -> new HashMap<>());
                    stop2Position.put(id, iStop);
                }
            }
        } catch (IOException e) {
            log.error("File reading error: " + e.getMessage());
        }

        long duration = System.currentTimeMillis() - startTime;
        log.info("!!1 Struct creation duration: " + duration);
        return res;
    }

    public boolean isThereDirection(int fromStop, int toStop) {
        Map<Integer, Integer> routes1 = store.get(fromStop);
        if (routes1 == null) return false;
        Map<Integer, Integer> routes2 = store.get(toStop);
        if (routes2 == null) return false;

        Set<Integer> r1 = routes1.keySet();
        Set<Integer> r2 = routes2.keySet();
        Sets.SetView<Integer> intersection = Sets.intersection(r1, r2);
        for (Integer i : intersection) {
            Integer positionFrom = routes1.get(i);
            Integer positionTo = routes2.get(i);
            if (positionFrom < positionTo) {
                return true;
            }
        }
        return false;
    }

}

