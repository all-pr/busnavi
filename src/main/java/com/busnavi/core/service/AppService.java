package com.busnavi.core.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

@Slf4j
@Service
public class AppService {

    public <T> T measure(Supplier<T> sup) {
        long start = System.currentTimeMillis();
        try {
            return sup.get();
        } finally {
            long duration = System.currentTimeMillis() - start;
            log.info("Duration={}", duration);
        }
    }

}
