package com.busnavi.core.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Route {

    private int x;
    private int y;
    private boolean direct;

    public Route() {
    }

    public Route(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Route(int x, int y, boolean direct) {
        this.x = x;
        this.y = y;
        this.direct = direct;
    }
}
