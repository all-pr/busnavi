package com.busnavi.core.controller;

import com.busnavi.core.model.Route;
import com.busnavi.core.service.AppService;
import com.busnavi.core.service.LinesEngine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Slf4j
@Controller
public class MainController {

    final LinesEngine linesEngine;
    final AppService appService;


    public MainController(LinesEngine linesLoader, AppService appService) {
        this.linesEngine = linesLoader;
        this.appService = appService;
    }

    @GetMapping(value = "/api/direct")
    public ResponseEntity<Route> isThereDirect(@RequestBody Route route) {
        log.info("calling isThereDirect method");
        log.info("Request Body: {}", route);

        boolean response = appService.measure(() -> linesEngine.isThereDirection(route.getX(), route.getY()));


        log.info("isThereDirect method has been finished");


        return ResponseEntity.ok(new Route(route.getX(), route.getY(), response));
    }
}
